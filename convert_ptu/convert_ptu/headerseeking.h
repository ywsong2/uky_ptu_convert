#pragma once

unsigned int HS_File_GUID;
unsigned int HS_File_AssuredContent;
unsigned int HS_CreatorSW_ContentVersion;
unsigned int HS_CreatorSW_Name;
unsigned int HS_CreatorSW_Version;
unsigned int HS_File_CreatingTime;
unsigned int HS_File_Comment;
unsigned int HS_Measurement_Mode;
unsigned int HS_Measurement_SubMode;
unsigned int HS_Fast_Load_End;
unsigned int HS_TTResultFormat_TTTRRecType;
unsigned int HS_TTResultFormat_BitsPerRecord;
unsigned int HS_MeasDesc_BinningFactor;
unsigned int HS_MeasDesc_Offset;
unsigned int HS_MeasDesc_AcquisitionTime;
unsigned int HS_MeasDesc_StopAt;
unsigned int HS_MeasDesc_StopOnOvfl;
unsigned int HS_MeasDesc_Restart;
unsigned int HS_CurSWSetting_DispLog;
unsigned int HS_CurSWSetting_DispAxisTimeFrom;
unsigned int HS_CurSWSetting_DispAxisTimeTo;
unsigned int HS_CurSWSetting_DispAxisCountFrom;
unsigned int HS_CurSWSetting_DispAxisCountTo;
unsigned int HS_CurSWSetting_DispCurves;
unsigned int HS_CurSWSetting_DispCurve_MapTo_0;
unsigned int HS_CurSWSetting_DispCurve_Show_0;
unsigned int HS_CurSWSetting_DispCurve_MapTo_1;
unsigned int HS_CurSWSetting_DispCurve_Show_1;
unsigned int HS_CurSWSetting_DispCurve_MapTo_2;
unsigned int HS_CurSWSetting_DispCurve_Show_2;
unsigned int HS_CurSWSetting_DispCurve_MapTo_3;
unsigned int HS_CurSWSetting_DispCurve_Show_3;
unsigned int HS_CurSWSetting_DispCurve_MapTo_4;
unsigned int HS_CurSWSetting_DispCurve_Show_4;
unsigned int HS_CurSWSetting_DispCurve_MapTo_5;
unsigned int HS_CurSWSetting_DispCurve_Show_5;
unsigned int HS_CurSWSetting_DispCurve_MapTo_6;
unsigned int HS_CurSWSetting_DispCurve_Show_6;
unsigned int HS_CurSWSetting_DispCurve_MapTo_7;
unsigned int HS_CurSWSetting_DispCurve_Show_7;
unsigned int HS_HW_Type;
unsigned int HS_HW_PartNo;
unsigned int HS_HW_Version;
unsigned int HS_HW_SerialNo;
unsigned int HS_HWSync_Divider;
unsigned int HS_HWSync_Offset;
unsigned int HS_HWSync_CFDZeroCross;
unsigned int HS_HWSync_CFDLevel;
unsigned int HS_HW_InpChannels;
unsigned int HS_HWInpChan_CFDZeroCross;
unsigned int HS_HWInpChan_CFDLevel;
unsigned int HS_MeasDesc_Resolution;
unsigned int HS_HW_BaseResolution;
unsigned int HS_HW_ExternalDevices;
unsigned int HS_HWRouter_ModelCode;
unsigned int HS_HWRouter_Channels;
unsigned int HS_HWRouter_Enabled;
unsigned int HS_HWRouterChan_InputType_0;
unsigned int HS_HWRouterChan_InputLevel_0;
unsigned int HS_HWRouterChan_RisingEdge_0;
unsigned int HS_HWRouterChan_Offset_0;
unsigned int HS_HWRouterChan_CFDPresent_0;
unsigned int HS_HWRouterChan_InputType_1;
unsigned int HS_HWRouterChan_InputLevel_1;
unsigned int HS_HWRouterChan_RisingEdge_1;
unsigned int HS_HWRouterChan_Offset_1;
unsigned int HS_HWRouterChan_CFDPresent_1;
unsigned int HS_HWRouterChan_InputType_2;
unsigned int HS_HWRouterChan_InputLevel_2;
unsigned int HS_HWRouterChan_RisingEdge_2;
unsigned int HS_HWRouterChan_Offset_2;
unsigned int HS_HWRouterChan_CFDPresent_2;
unsigned int HS_HWRouterChan_InputType_3;
unsigned int HS_HWRouterChan_InputLevel_3;
unsigned int HS_HWRouterChan_RisingEdge_3;
unsigned int HS_HWRouterChan_Offset_3;
unsigned int HS_HWRouterChan_CFDPresent_3;
unsigned int HS_HW_Markers;
unsigned int HS_HWMarkers_RisingEdge_0;
unsigned int HS_HWMarkers_RisingEdge_1;
unsigned int HS_HWMarkers_RisingEdge_2;
unsigned int HS_HWMarkers_RisingEdge_3;
unsigned int HS_HWMarkers_Enabled_0;
unsigned int HS_HWMarkers_Enabled_1;
unsigned int HS_HWMarkers_Enabled_2;
unsigned int HS_HWMarkers_Enabled_3;
unsigned int HS_HWMarkers_HoldOff;
unsigned int HS_MeasDesc_GlobalResolution;
unsigned int HS_TTResult_SyncRate;
unsigned int HS_TTResult_InputRate;
unsigned int HS_TTResult_StopAfter;
unsigned int HS_TTResult_NumberOfRecords;
unsigned int HS_Header_End;