#include "main.h"
#include "headerseeking.h"
#include "config.h"

using namespace PRIORLib;

// Create PTU header 
// input params:	int scan_counter - Current scan number
// return params:	FILE* fp - Created ptu file pointer 
FILE* Create_Header(char targetFileName[]) {
	FILE* converted_ptu_fpout = NULL;

	if ((converted_ptu_fpout = fopen(targetFileName, "wb")) == NULL)
	{
		printf("\n ERROR! Output PTU file cannot be opened, aborting.\n");
		exit(0);
	}

	// Pre-defined buffers
	char Magic[8];
	char Version[8];
	char Buffer[40];
	char* AnsiBuffer;
	WCHAR* WideBuffer;
	int Result;

	// Get current time
	const int EpochDiff = 25569;
	long long currentTime = (long long)time(NULL);
	double daysSinceEpoch = EpochDiff + (currentTime / (60.0 * 60 * 24));
	long long _File_CreatingTime = *((long long*)&daysSinceEpoch);

	// Create UUID
	UUID uuid;
	UuidCreate(&uuid);
	char uuid_with_brace[39];	// UUID has fixed size of 36 and 2 for bracers and 1 for null-end
	char* uuid_str;
	UuidToStringA(&uuid, (RPC_CSTR*)&uuid_str);
	uuid_with_brace[0] = '{';
	uuid_with_brace[37] = '}';
	uuid_with_brace[38] = NULL;

	for (int i = 0; i < strlen(uuid_str); i++) {
		uuid_with_brace[i + 1] = uuid_str[i];
	}

	// Start to write default values on header (overwrite after each scan)
	struct TgHd File_GUID = { "File_GUID", -1, tyAnsiString, 40 };								// {E1423E78-E760-47A2-33A4-31CB670DBFEF}
	struct TgHd File_AssuredContent = { "File_AssuredContent", -1, tyAnsiString, 32 };			// PicoHarp 300: HWSETG SWSETG
	struct TgHd CreatorSW_ContentVersion = { "CreatorSW_ContentVersion", -1, tyAnsiString, 8 };	// 3.0	
	struct TgHd CreatorSW_Name = { "CreatorSW_Name", -1, tyAnsiString, 24 };					// PicoHarp Software
	struct TgHd CreatorSW_Version = { "CreatorSW_Version", -1, tyAnsiString, 8 };				// 3.0.0.3
	struct TgHd File_CreatingTime = { "File_CreatingTime", -1, tyTDateTime, _File_CreatingTime };
	struct TgHd File_Comment = { "File_Comment", -1, tyAnsiString, 8 };							// T3 Mode
	struct TgHd Measurement_Mode = { "Measurement_Mode", -1, tyInt8, CONF_Acq_Mode };			// 3 (T3 Mode)
	struct TgHd Measurement_SubMode = { "Measurement_SubMode", -1, tyInt8, 0 };
	struct TgHd TTResult_StopReason = { "TTResult_StopReason", -1, tyInt8, 0 };
	struct TgHd Fast_Load_End = { "Fast_Load_End", -1, tyEmpty8, 0 };
	struct TgHd TTResultFormat_TTTRRecType = { "TTResultFormat_TTTRRecType", -1, tyInt8, 66307 };
	struct TgHd TTResultFormat_BitsPerRecord = { "TTResultFormat_BitsPerRecord", -1, tyInt8, 32 };
	struct TgHd MeasDesc_BinningFactor = { "MeasDesc_BinningFactor", -1, tyInt8, CONF_Acq_Binning };
	struct TgHd MeasDesc_Offset = { "MeasDesc_Offset", -1, tyInt8, 0 };
	struct TgHd MeasDesc_AcquisitionTime = { "MeasDesc_AcquisitionTime", -1, tyInt8, CONF_Acq_Time };	
	struct TgHd MeasDesc_StopAt = { "MeasDesc_StopAt", -1, tyInt8, 65535 };
	struct TgHd MeasDesc_StopOnOvfl = { "MeasDesc_StopOnOvfl", -1, tyBool8, 0 };
	struct TgHd MeasDesc_Restart = { "MeasDesc_Restart", -1, tyBool8, 0 };
	struct TgHd CurSWSetting_DispLog = { "CurSWSetting_DispLog", -1, tyBool8, -1 };
	struct TgHd CurSWSetting_DispAxisTimeFrom = { "CurSWSetting_DispAxisTimeFrom", -1, tyInt8, 0 };
	struct TgHd CurSWSetting_DispAxisTimeTo = { "CurSWSetting_DispAxisTimeTo", -1, tyInt8, 262 };
	struct TgHd CurSWSetting_DispAxisCountFrom = { "CurSWSetting_DispAxisCountFrom", -1, tyInt8, 0 };
	struct TgHd CurSWSetting_DispAxisCountTo = { "CurSWSetting_DispAxisCountTo", -1, tyInt8, 1000000 };
	struct TgHd CurSWSetting_DispCurves = { "CurSWSetting_DispCurves", -1, tyInt8, 8 };
	struct TgHd CurSWSetting_DispCurve_MapTo_0 = { "CurSWSetting_DispCurve_MapTo", 0, tyInt8, 0 };
	struct TgHd CurSWSetting_DispCurve_Show_0 = { "CurSWSetting_DispCurve_Show", 0, tyBool8, -1 };
	struct TgHd CurSWSetting_DispCurve_MapTo_1 = { "CurSWSetting_DispCurve_MapTo", 1, tyInt8, 1 };
	struct TgHd CurSWSetting_DispCurve_Show_1 = { "CurSWSetting_DispCurve_Show", 1, tyBool8, -1 };
	struct TgHd CurSWSetting_DispCurve_MapTo_2 = { "CurSWSetting_DispCurve_MapTo", 2, tyInt8, 2 };
	struct TgHd CurSWSetting_DispCurve_Show_2 = { "CurSWSetting_DispCurve_Show", 2, tyBool8, -1 };
	struct TgHd CurSWSetting_DispCurve_MapTo_3 = { "CurSWSetting_DispCurve_MapTo", 3, tyInt8, 3 };
	struct TgHd CurSWSetting_DispCurve_Show_3 = { "CurSWSetting_DispCurve_Show", 3, tyBool8, -1 };
	struct TgHd CurSWSetting_DispCurve_MapTo_4 = { "CurSWSetting_DispCurve_MapTo", 4, tyInt8, 4 };
	struct TgHd CurSWSetting_DispCurve_Show_4 = { "CurSWSetting_DispCurve_Show", 4, tyBool8, -1 };
	struct TgHd CurSWSetting_DispCurve_MapTo_5 = { "CurSWSetting_DispCurve_MapTo", 5, tyInt8, 5 };
	struct TgHd CurSWSetting_DispCurve_Show_5 = { "CurSWSetting_DispCurve_Show", 5, tyBool8, -1 };
	struct TgHd CurSWSetting_DispCurve_MapTo_6 = { "CurSWSetting_DispCurve_MapTo", 6, tyInt8, 6 };
	struct TgHd CurSWSetting_DispCurve_Show_6 = { "CurSWSetting_DispCurve_Show", 6, tyBool8, -1 };
	struct TgHd CurSWSetting_DispCurve_MapTo_7 = { "CurSWSetting_DispCurve_MapTo", 7, tyInt8, 7 };
	struct TgHd CurSWSetting_DispCurve_Show_7 = { "CurSWSetting_DispCurve_Show", 7, tyBool8, -1 };
	struct TgHd HW_Type = { "HW_Type", -1, tyAnsiString, 16 };			// "PicoHarp 300"
	struct TgHd HW_PartNo = { "HW_PartNo", -1, tyAnsiString, 8 };	
	struct TgHd HW_Version = { "HW_Version", -1, tyAnsiString, 8 };	
	struct TgHd HW_SerialNo = { "HW_SerialNo", -1, tyAnsiString, 8 };
	struct TgHd HWSync_Divider = { "HWSync_Divider", -1, tyInt8, CONF_Acq_Sync_Divider };
	struct TgHd HWSync_Offset = { "HWSync_Offset", -1, tyInt8, 0 };
	struct TgHd HWSync_CFDZeroCross = { "HWSync_CFDZeroCross", -1, tyInt8, CONF_Acq_CFD_Zero_Cross_0 };
	struct TgHd HWSync_CFDLevel = { "HWSync_CFDLevel", -1, tyInt8, CONF_Acq_CFD_Level_0 };
	struct TgHd HW_InpChannels = { "HW_InpChannels", -1, tyInt8, 1 };
	struct TgHd HWInpChan_CFDZeroCross = { "HWInpChan_CFDZeroCross", 0, tyInt8, CONF_Acq_CFD_Zero_Cross_1 };
	struct TgHd HWInpChan_CFDLevel = { "HWInpChan_CFDLevel", 0, tyInt8, CONF_Acq_CFD_Level_1 };
	struct TgHd MeasDesc_Resolution = { "MeasDesc_Resolution", -1, tyFloat8, 4436493793489709585 };	// Put temporary values
	struct TgHd HW_BaseResolution = { "HW_BaseResolution", -1, tyFloat8, 4436493793489709585 };		// Put temporary values (4 ps) = 4.0 * E-12
	struct TgHd HW_ExternalDevices = { "HW_ExternalDevices", -1, tyInt8, 3 };
	struct TgHd HWRouter_ModelCode = { "HWRouter_ModelCode", -1, tyInt8, 3 };
	struct TgHd HWRouter_Channels = { "HWRouter_Channels", -1, tyInt8, 4 };
	struct TgHd HWRouter_Enabled = { "HWRouter_Enabled", -1, tyBool8, 0 };
	struct TgHd HWRouterChan_InputType_0 = { "HWRouterChan_InputType", 0, tyInt8, 2 };
	struct TgHd HWRouterChan_InputLevel_0 = { "HWRouterChan_InputLevel", 0, tyInt8, 1500 };
	struct TgHd HWRouterChan_RisingEdge_0 = { "HWRouterChan_RisingEdge", 0, tyBool8, -1 };
	struct TgHd HWRouterChan_Offset_0 = { "HWRouterChan_Offset", 0, tyInt8, 0 };
	struct TgHd HWRouterChan_CFDPresent_0 = { "HWRouterChan_CFDPresent", 0, tyBool8, 0 };
	struct TgHd HWRouterChan_InputType_1 = { "HWRouterChan_InputType", 1, tyInt8, 2 };
	struct TgHd HWRouterChan_InputLevel_1 = { "HWRouterChan_InputLevel", 1, tyInt8, 1500 };
	struct TgHd HWRouterChan_RisingEdge_1 = { "HWRouterChan_RisingEdge", 1, tyBool8, -1 };
	struct TgHd HWRouterChan_Offset_1 = { "HWRouterChan_Offset", 1, tyInt8, 0 };
	struct TgHd HWRouterChan_CFDPresent_1 = { "HWRouterChan_CFDPresent", 1, tyBool8, 0 };
	struct TgHd HWRouterChan_InputType_2 = { "HWRouterChan_InputType", 2, tyInt8, 2 };
	struct TgHd HWRouterChan_InputLevel_2 = { "HWRouterChan_InputLevel", 2, tyInt8, 1500 };
	struct TgHd HWRouterChan_RisingEdge_2 = { "HWRouterChan_RisingEdge", 2, tyBool8, -1 };
	struct TgHd HWRouterChan_Offset_2 = { "HWRouterChan_Offset", 2, tyInt8, 0 };
	struct TgHd HWRouterChan_CFDPresent_2 = { "HWRouterChan_CFDPresent", 2, tyBool8, 0 };
	struct TgHd HWRouterChan_InputType_3 = { "HWRouterChan_InputType", 3, tyInt8, 2 };
	struct TgHd HWRouterChan_InputLevel_3 = { "HWRouterChan_InputLevel", 3, tyInt8, 1500 };
	struct TgHd HWRouterChan_RisingEdge_3 = { "HWRouterChan_RisingEdge", 3, tyBool8, -1 };
	struct TgHd HWRouterChan_Offset_3 = { "HWRouterChan_Offset", 3, tyInt8, 0 };
	struct TgHd HWRouterChan_CFDPresent_3 = { "HWRouterChan_CFDPresent", 3, tyBool8, 0 };
	struct TgHd HW_Markers = { "HW_Markers", -1, tyInt8, 4 };
	struct TgHd HWMarkers_RisingEdge_0 = { "HWMarkers_RisingEdge", 0, tyBool8, 0 };
	struct TgHd HWMarkers_RisingEdge_1 = { "HWMarkers_RisingEdge", 1, tyBool8, 0 };
	struct TgHd HWMarkers_RisingEdge_2 = { "HWMarkers_RisingEdge", 2, tyBool8, 0 };
	struct TgHd HWMarkers_RisingEdge_3 = { "HWMarkers_RisingEdge", 3, tyBool8, 0 };
	struct TgHd HWMarkers_Enabled_0 = { "HWMarkers_Enabled", 0, tyBool8, -1 };
	struct TgHd HWMarkers_Enabled_1 = { "HWMarkers_Enabled", 1, tyBool8, -1 };
	struct TgHd HWMarkers_Enabled_2 = { "HWMarkers_Enabled", 2, tyBool8, -1 };
	struct TgHd HWMarkers_Enabled_3 = { "HWMarkers_Enabled", 3, tyBool8, -1 };
	struct TgHd HWMarkers_HoldOff = { "HWMarkers_HoldOff", -1, tyInt8, 0 };
	struct TgHd MeasDesc_GlobalResolution = { "MeasDesc_GlobalResolution", -1, tyFloat8, 4488857147475206489 };	// Put temporary values
	struct TgHd TTResult_SyncRate = { "TTResult_SyncRate", -1, tyInt8, 19400000 };								// Put temporary values
	struct TgHd TTResult_InputRate = { "TTResult_InputRate", 0, tyInt8, 317523 };								// Put temporary values
	struct TgHd TTResult_StopAfter = { "TTResult_StopAfter", -1, tyInt8, CONF_Acq_Time };
	struct TgHd TTResult_NumberOfRecords = { "TTResult_NumberOfRecords", -1, tyInt8, 2578789 };					// Put temporary values
	struct TgHd Header_End = { "Header_End", -1, tyEmpty8, 0 };

	unsigned int fp_offset = 0;
	unsigned int written_number_of_bytes = 0;

	// Fixed strings for PicoHarp TTTR (May need to change later)
	char header_magic[] = "PQTTTR";
	char header_version[] = "1.0.00";

	// Magic header
	int padding_length = 8 - (sizeof(header_magic) % 8) + 1;
	fwrite(header_magic, 1, sizeof(header_magic) - 1, converted_ptu_fpout);
	fwrite("\0", padding_length, 1, converted_ptu_fpout);
	fp_offset = fp_offset + 8;

	// Version
	padding_length = 8 - (sizeof(header_version) % 8) + 1;
	fwrite(header_version, 1, sizeof(header_version) - 1, converted_ptu_fpout);
	fwrite("\0", padding_length, 1, converted_ptu_fpout);
	fp_offset = fp_offset + 8;

	HS_File_GUID = fp_offset; fp_offset += write_header(converted_ptu_fpout, File_GUID, uuid_with_brace);
	HS_File_AssuredContent = fp_offset; fp_offset += write_header(converted_ptu_fpout, File_AssuredContent, "PicoHarp 300: HWSETG SWSETG");	// Change it if it is different for your system
	HS_CreatorSW_ContentVersion = fp_offset; fp_offset += write_header(converted_ptu_fpout, CreatorSW_ContentVersion, "3.0"); // Change it if it is different for your system
	HS_CreatorSW_Name = fp_offset; fp_offset += write_header(converted_ptu_fpout, CreatorSW_Name, "PicoHarp Software"); // Change it if it is different for your system
	HS_CreatorSW_Version = fp_offset; fp_offset += write_header(converted_ptu_fpout, CreatorSW_Version, "3.0.0.3"); // Change it if it is different for your system
	HS_File_CreatingTime = fp_offset; fp_offset += write_header(converted_ptu_fpout, File_CreatingTime, ""); 
	HS_File_Comment = fp_offset; fp_offset += write_header(converted_ptu_fpout, File_Comment, "T3 Mode"); 
	HS_Measurement_Mode = fp_offset; fp_offset += write_header(converted_ptu_fpout, Measurement_Mode, "");
	HS_Measurement_SubMode = fp_offset; fp_offset += write_header(converted_ptu_fpout, Measurement_SubMode, "");
	HS_Measurement_SubMode = fp_offset; fp_offset += write_header(converted_ptu_fpout, TTResult_StopReason, "");
	HS_Fast_Load_End = fp_offset; fp_offset += write_header(converted_ptu_fpout, Fast_Load_End, "");
	HS_TTResultFormat_TTTRRecType = fp_offset; fp_offset += write_header(converted_ptu_fpout, TTResultFormat_TTTRRecType, "");
	HS_TTResultFormat_BitsPerRecord = fp_offset; fp_offset += write_header(converted_ptu_fpout, TTResultFormat_BitsPerRecord, "");
	HS_MeasDesc_BinningFactor = fp_offset; fp_offset += write_header(converted_ptu_fpout, MeasDesc_BinningFactor, "");
	HS_MeasDesc_Offset = fp_offset; fp_offset += write_header(converted_ptu_fpout, MeasDesc_Offset, "");
	HS_MeasDesc_AcquisitionTime = fp_offset; fp_offset += write_header(converted_ptu_fpout, MeasDesc_AcquisitionTime, "");
	HS_MeasDesc_StopAt = fp_offset; fp_offset += write_header(converted_ptu_fpout, MeasDesc_StopAt, "");
	HS_MeasDesc_StopOnOvfl = fp_offset; fp_offset += write_header(converted_ptu_fpout, MeasDesc_StopOnOvfl, "");
	HS_MeasDesc_Restart = fp_offset; fp_offset += write_header(converted_ptu_fpout, MeasDesc_Restart, "");
	HS_CurSWSetting_DispLog = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispLog, "");
	HS_CurSWSetting_DispAxisTimeFrom = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispAxisTimeFrom, "");
	HS_CurSWSetting_DispAxisTimeTo = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispAxisTimeTo, "");
	HS_CurSWSetting_DispAxisCountFrom = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispAxisCountFrom, "");
	HS_CurSWSetting_DispAxisCountTo = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispAxisCountTo, "");
	HS_CurSWSetting_DispCurves = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurves, "");
	HS_CurSWSetting_DispCurve_MapTo_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_MapTo_0, "");
	HS_CurSWSetting_DispCurve_Show_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_Show_0, "");
	HS_CurSWSetting_DispCurve_MapTo_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_MapTo_1, "");
	HS_CurSWSetting_DispCurve_Show_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_Show_1, "");
	HS_CurSWSetting_DispCurve_MapTo_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_MapTo_2, "");
	HS_CurSWSetting_DispCurve_Show_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_Show_2, "");
	HS_CurSWSetting_DispCurve_MapTo_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_MapTo_3, "");
	HS_CurSWSetting_DispCurve_Show_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_Show_3, "");
	HS_CurSWSetting_DispCurve_MapTo_4 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_MapTo_4, "");
	HS_CurSWSetting_DispCurve_Show_4 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_Show_4, "");
	HS_CurSWSetting_DispCurve_MapTo_5 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_MapTo_5, "");
	HS_CurSWSetting_DispCurve_Show_5 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_Show_5, "");
	HS_CurSWSetting_DispCurve_MapTo_6 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_MapTo_6, "");
	HS_CurSWSetting_DispCurve_Show_6 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_Show_6, "");
	HS_CurSWSetting_DispCurve_MapTo_7 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_MapTo_7, "");
	HS_CurSWSetting_DispCurve_Show_7 = fp_offset; fp_offset += write_header(converted_ptu_fpout, CurSWSetting_DispCurve_Show_7, "");
	HS_HW_Type = fp_offset; fp_offset += write_header(converted_ptu_fpout, HW_Type, "PicoHarp 300"); // Change it if it is different for your system
	HS_HW_PartNo = fp_offset; fp_offset += write_header(converted_ptu_fpout, HW_PartNo, "930004"); // Change it if it is different for your system
	HS_HW_Version = fp_offset; fp_offset += write_header(converted_ptu_fpout, HW_Version, "2.0"); // Change it if it is different for your system
	HS_HW_SerialNo = fp_offset; fp_offset += write_header(converted_ptu_fpout, HW_SerialNo, "1234567"); // Change it if it is different for your system
	HS_HWSync_Divider = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWSync_Divider, "");
	HS_HWSync_Offset = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWSync_Offset, "");
	HS_HWSync_CFDZeroCross = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWSync_CFDZeroCross, "");
	HS_HWSync_CFDLevel = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWSync_CFDLevel, "");
	HS_HW_InpChannels = fp_offset; fp_offset += write_header(converted_ptu_fpout, HW_InpChannels, "");
	HS_HWInpChan_CFDZeroCross = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWInpChan_CFDZeroCross, "");
	HS_HWInpChan_CFDLevel = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWInpChan_CFDLevel, "");
	HS_MeasDesc_Resolution = fp_offset; fp_offset += write_header(converted_ptu_fpout, MeasDesc_Resolution, "");
	HS_HW_BaseResolution = fp_offset; fp_offset += write_header(converted_ptu_fpout, HW_BaseResolution, "");
	HS_HW_ExternalDevices = fp_offset; fp_offset += write_header(converted_ptu_fpout, HW_ExternalDevices, "");
	HS_HWRouter_ModelCode = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouter_ModelCode, "");
	HS_HWRouter_Channels = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouter_Channels, "");
	HS_HWRouter_Enabled = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouter_Enabled, "");
	HS_HWRouterChan_InputType_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_InputType_0, "");
	HS_HWRouterChan_InputLevel_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_InputLevel_0, "");
	HS_HWRouterChan_RisingEdge_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_RisingEdge_0, "");
	HS_HWRouterChan_Offset_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_Offset_0, "");
	HS_HWRouterChan_CFDPresent_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_CFDPresent_0, "");
	HS_HWRouterChan_InputType_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_InputType_1, "");
	HS_HWRouterChan_InputLevel_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_InputLevel_1, "");
	HS_HWRouterChan_RisingEdge_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_RisingEdge_1, "");
	HS_HWRouterChan_Offset_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_Offset_1, "");
	HS_HWRouterChan_CFDPresent_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_CFDPresent_1, "");
	HS_HWRouterChan_InputType_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_InputType_2, "");
	HS_HWRouterChan_InputLevel_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_InputLevel_2, "");
	HS_HWRouterChan_RisingEdge_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_RisingEdge_2, "");
	HS_HWRouterChan_Offset_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_Offset_2, "");
	HS_HWRouterChan_CFDPresent_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_CFDPresent_2, "");
	HS_HWRouterChan_InputType_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_InputType_3, "");
	HS_HWRouterChan_InputLevel_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_InputLevel_3, "");
	HS_HWRouterChan_RisingEdge_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_RisingEdge_3, "");
	HS_HWRouterChan_Offset_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_Offset_3, "");
	HS_HWRouterChan_CFDPresent_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWRouterChan_CFDPresent_3, "");
	HS_HW_Markers = fp_offset; fp_offset += write_header(converted_ptu_fpout, HW_Markers, "");
	HS_HWMarkers_RisingEdge_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_RisingEdge_0, "");
	HS_HWMarkers_RisingEdge_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_RisingEdge_1, "");
	HS_HWMarkers_RisingEdge_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_RisingEdge_2, "");
	HS_HWMarkers_RisingEdge_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_RisingEdge_3, "");
	HS_HWMarkers_Enabled_0 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_Enabled_0, "");
	HS_HWMarkers_Enabled_1 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_Enabled_1, "");
	HS_HWMarkers_Enabled_2 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_Enabled_2, "");
	HS_HWMarkers_Enabled_3 = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_Enabled_3, "");
	HS_HWMarkers_HoldOff = fp_offset; fp_offset += write_header(converted_ptu_fpout, HWMarkers_HoldOff, "");
	HS_MeasDesc_GlobalResolution = fp_offset; fp_offset += write_header(converted_ptu_fpout, MeasDesc_GlobalResolution, "");
	HS_TTResult_SyncRate = fp_offset; fp_offset += write_header(converted_ptu_fpout, TTResult_SyncRate, "");
	HS_TTResult_InputRate = fp_offset; fp_offset += write_header(converted_ptu_fpout, TTResult_InputRate, "");
	HS_TTResult_StopAfter = fp_offset; fp_offset += write_header(converted_ptu_fpout, TTResult_StopAfter, "");
	HS_TTResult_NumberOfRecords = fp_offset; fp_offset += write_header(converted_ptu_fpout, TTResult_NumberOfRecords, "");
	HS_Header_End = fp_offset; fp_offset += write_header(converted_ptu_fpout, Header_End, "");

	// Release memory assigned for UUID
	RpcStringFreeA((RPC_CSTR*)&uuid_str);

	return converted_ptu_fpout;
}

int main(int argc, char* argv[])
{
	// Check input arguments
	if ((argc<2) || (argc>2))
	{
		printf("usage: ph_multiscan.exe config_file_name\n");
		_getch();
		exit(-1);
	}

	// Global flags
	bool prior_stage_enabled = true;

	// Global variable to get settings from PicoHarp
	long long _MeasDesc_Resolution = 0;
	long long _HW_BaseResolution = 0;
	long long _MeasDesc_GlobalResolution = 0;
	int _TTResult_SyncRate = 0;
	int _TTResult_InputRate = 0;
	int _TTResult_NumberOfRecords = 0;

	// Global file pointer 
	FILE* fp_target_ptu = NULL;

	/***************************************************************************************************/
	// Load config - START
	FILE* fp_config;
	char conf_buffer[CONFIG_LINE_BUFFER_SIZE];

	if ((fp_config = fopen(argv[1], "r")) == NULL) {
		printf("\ncannot open config.txt file\n");
		exit(0);
	}
	else
	{
		while (!feof(fp_config))
		{
			fgets(conf_buffer, CONFIG_LINE_BUFFER_SIZE, fp_config);
			if (conf_buffer[0] == '#') {
				continue;
			}
			if (strstr(conf_buffer, "ACQ_TIME")) {
				CONF_Acq_Time = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "ACQ_MODE")) {
				CONF_Acq_Mode = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "ACQ_BINNING")) {
				CONF_Acq_Binning = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "ACQ_OFFSET")) {
				CONF_Acq_Offset = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "ACQ_SYNC_DIVIDER")) {
				CONF_Acq_Sync_Divider = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "ACQ_CFD_ZERO_CROSS_0")) {
				CONF_Acq_CFD_Zero_Cross_0 = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "ACQ_CFD_LEVEL_0")) {
				CONF_Acq_CFD_Level_0 = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "ACQ_CFD_ZERO_CROSS_1")) {
				CONF_Acq_CFD_Zero_Cross_1 = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "ACQ_CFD_LEVEL_1")) {
				CONF_Acq_CFD_Level_1 = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "NUMBER_OF_SCAN_X")) {
				CONF_Num_Scan_X = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "NUMBER_OF_SCAN_Y")) {
				CONF_Num_Scan_Y = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "PRI_ENABLED")) {
				CONF_Prior_Enabled = read_int_from_config_line(conf_buffer);
				if (CONF_Prior_Enabled == 0) {
					prior_stage_enabled = false;
				}
			}
			if (strstr(conf_buffer, "PRI_COMPORT_NUBMER")) {
				CONF_Prior_COMPort_Number = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "PRI_START_X")) {
				CONF_Prior_Start_X = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "PRI_START_Y")) {
				CONF_Prior_Start_Y = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "PRI_STEP_X")) {
				CONF_Prior_Step_X = read_int_from_config_line(conf_buffer);
			}
			if (strstr(conf_buffer, "PRI_STEP_Y")) {
				CONF_Prior_Step_Y = read_int_from_config_line(conf_buffer);
			}
		}
	}

	fclose(fp_config);
	// Load config - END
	/***************************************************************************************************/

	/***************************************************************************************************/
	// Setting up Prior stage - START
	ScanClass^ prior_scan;
	StageClass^ prior_xyStage;
	ZClass^ prior_zStage;

	prior_scan = gcnew PRIORLib::ScanClass();
	prior_xyStage = gcnew PRIORLib::StageClass();
	prior_zStage = gcnew PRIORLib::ZClass();

	// Open up Prior stage
	if (prior_stage_enabled) {
		int^ targetPortNum = CONF_Prior_COMPort_Number;
		if (prior_scan->Connect(*targetPortNum) != PRIORLib::Constants::PRIOR_OK) {
			printf("\n ERROR: Failed to connect Prior stage! Check your COM Port number in config.txt!\n");
			exit(0);
		}
	}
	// Setting up Prior stage - END
	/***************************************************************************************************/

	/***************************************************************************************************/
	// Setting up PicoHarp - START
	char LIB_Version_CharBuffer[8];
	char HW_Model_CharBuffer[16];
	char HW_PartNo_CharBuffer[8];
	char HW_Version_CharBuffer[8];
	char HW_Serial_CharBuffer[8];
	char Errorstring_CharBuffer[40];
	int Mode = CONF_Acq_Mode;
	int Binning = CONF_Acq_Binning;
	int Offset = CONF_Acq_Offset;
	int Tacq = CONF_Acq_Time;
	int SyncDivider = CONF_Acq_Sync_Divider;
	int CFDZeroCross0 = CONF_Acq_CFD_Zero_Cross_0;
	int CFDLevel0 = CONF_Acq_CFD_Level_0;
	int CFDZeroCross1 = CONF_Acq_CFD_Zero_Cross_1;
	int CFDLevel1 = CONF_Acq_CFD_Level_1;
	int blocksz = TTREADMAX;// in steps of 512
	double Resolution, BaseResolution;
	int Countrate0;
	int Countrate1;
	int Progress;
	int dev[MAXDEVNUM];
	int found = 0;
	FILE *fpout;
	int retcode;
	int flags;
	int nactual;
	int FiFoWasFull, CTCDone;

	printf("\nPicoHarp 300 PHLib.DLL   TTTR Mode Demo    M. Wahl, PicoQuant GmbH, 2013");
	printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	PH_GetLibraryVersion(LIB_Version_CharBuffer);
	printf("\nPHLIB.DLL version is %s", LIB_Version_CharBuffer);
	if (strncmp(LIB_Version_CharBuffer, LIB_VERSION, sizeof(LIB_VERSION)) != 0)
		printf("\nWarning: The application was built for version %s.", LIB_VERSION);

	printf("\n\n");
	printf("Mode             : %ld\n", Mode);
	printf("Binning          : %ld\n", Binning);
	printf("Offset           : %ld\n", Offset);
	printf("AcquisitionTime  : %ld\n", Tacq);
	printf("SyncDivider      : %ld\n", SyncDivider);
	printf("CFDZeroCross0    : %ld\n", CFDZeroCross0);
	printf("CFDLevel0        : %ld\n", CFDLevel0);
	printf("CFDZeroCross1    : %ld\n", CFDZeroCross1);
	printf("CFDLevel1        : %ld\n", CFDLevel1);

	printf("\nSearching for PicoHarp devices...");
	printf("\nDevidx     Status");

	for (int i = 0;i<MAXDEVNUM;i++)
	{
		retcode = PH_OpenDevice(i, HW_Serial_CharBuffer);
		if (retcode == 0) //Grab any PicoHarp we can open
		{
			printf("\n  %1d        S/N %s", i, HW_Serial_CharBuffer);
			dev[found] = i; //keep index to devices we want to use
			found++;
		}
		else
		{
			if (retcode == ERROR_DEVICE_OPEN_FAIL)
				printf("\n  %1d        no device", i);
			else
			{
				PH_GetErrorString(Errorstring_CharBuffer, retcode);
				printf("\n  %1d        %s", i, Errorstring_CharBuffer);
			}
		}
	}

	if (found<1)
	{
		printf("\n ERROR:: No device available.\n");
		exit(0);
	}

	printf("\nUsing device #%1d", dev[0]);
	printf("\nInitializing the device...");

	retcode = PH_Initialize(dev[0], Mode);
	if (retcode<0)
	{
		printf("\n ERROR: PH init error %d. Aborted.\n", retcode);
		exit(0);
	}

	retcode = PH_GetHardwareInfo(dev[0], HW_Model_CharBuffer, HW_PartNo_CharBuffer, HW_Version_CharBuffer); 
	if (retcode<0)
	{
		printf("\n ERROR: PH_GetHardwareInfo error %d. Aborted.\n", retcode);
		exit(0);
	}
	else
		printf("\n ERROR: Found Model %s Partnum %s Version %s", HW_Model_CharBuffer, HW_PartNo_CharBuffer, HW_Version_CharBuffer);

	printf("\nCalibrating...");
	retcode = PH_Calibrate(dev[0]);
	if (retcode<0)
	{
		printf("\n ERROR: Calibration Error %d. Aborted.\n", retcode);
		exit(0);
	}

	retcode = PH_SetSyncDiv(dev[0], SyncDivider);
	if (retcode<0)
	{
		printf("\n ERROR: PH_SetSyncDiv error %ld. Aborted.\n", retcode);
		exit(0);
	}

	retcode = PH_SetInputCFD(dev[0], 0, CFDLevel0, CFDZeroCross0);
	if (retcode<0)
	{
		printf("\n ERROR: PH_SetInputCFD error %ld. Aborted.\n", retcode);
		exit(0);
	}

	retcode = PH_SetInputCFD(dev[0], 1, CFDLevel1, CFDZeroCross1);
	if (retcode<0)
	{
		printf("\n ERROR: PH_SetInputCFD error %ld. Aborted.\n", retcode);
		exit(0);
	}

	retcode = PH_SetBinning(dev[0], Binning);
	if (retcode<0)
	{
		printf("\n ERROR: PH_SetBinning error %d. Aborted.\n", retcode);
		exit(0);
	}

	retcode = PH_SetOffset(dev[0], Offset);
	if (retcode<0)
	{
		printf("\n ERROR: PH_SetOffset error %d. Aborted.\n", retcode);
		exit(0);
	}
	// Setting up PicoHarp - END
	/***************************************************************************************************/


	/***************************************************************************************************/
	// Perform multiple continuous scans - START
	int total_number_of_scans = 1;
	if (prior_stage_enabled) {
		total_number_of_scans = CONF_Num_Scan_X * CONF_Num_Scan_Y;
	}

	// Create target folder
	// Get current time stamp
	time_t currentTime = time(NULL);
	struct tm cur_tm = *localtime(&currentTime);
	char targetFolderName[256];
	sprintf(targetFolderName, "%d_%d_%d-%d_%d_%d", cur_tm.tm_year + 1900,
		cur_tm.tm_mon + 1,
		cur_tm.tm_mday,
		cur_tm.tm_hour,
		cur_tm.tm_min,
		cur_tm.tm_sec);

	// How much will Prior stage move after full scans?
	int totalMovedX = 0;
	int totalMovedY = 0;

	// Create directory
	if (CreateDirectoryA(targetFolderName, NULL) || ERROR_ALREADY_EXISTS == GetLastError()) {

		// Perform continuous scans
		for (int i = 0; i < total_number_of_scans; i++) {

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			int current_column = i % CONF_Num_Scan_X;
			int current_row = i / CONF_Num_Scan_X;

			// Relative moving amount from current position
			int targetX = 0;
			int targetY = 0;

			// Step #0: Prepare target file name
			char targetFileName[512];
			sprintf(targetFileName, "%s/%03d_%03d__%03d.ptu", targetFolderName, current_row, current_column, i);

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			// Step #1: Move Prior stage to the position
			if (prior_stage_enabled) {

				// Adjust relative moving amount based on current location
				if (i > 0) {
					if (i % CONF_Num_Scan_X == 0) {
						// When it changes rows, it needs to change moving direction of prior stage -> to <- (or <- to ->)
						targetY = CONF_Prior_Step_Y;
						CONF_Prior_Step_X = CONF_Prior_Step_X * (-1);
					}
					else {
						targetX = CONF_Prior_Step_X;
					}
				}

				printf("i = %d, targetX = %d, targetY = %d\n", i, targetX, targetY);

				// Remember how much it moved
				totalMovedX = totalMovedX + targetX;
				totalMovedY = totalMovedY + targetY;

				if (prior_xyStage->MoveRelative((double)targetX, (double)targetY) != PRIORLib::Constants::PRIOR_OK) {
					printf("\n ERROR: Failed to move PRIOR stage!\n");
				}

				// Wait until servo stops moving!
				int^ status = 0;
				prior_scan->IsMoving(*status);
				while (*status != 0) {
					Sleep(100);
					prior_scan->IsMoving(*status);
				}
			}

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			// Step #2: Create PTU header with default values
			fp_target_ptu = Create_Header(targetFileName);

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			// Step #3: Perform PicoHarp Scan
			retcode = PH_GetResolution(dev[0], &Resolution);
			if (retcode<0)
			{
				printf("\n ERROR: PH_GetResolution error %d. Aborted.\n", retcode);
				exit(0);
			}
			else
			{
				// Got MeasDesc_Resolution (Assumuption)
				_MeasDesc_Resolution = *((long long*)&Resolution);
			}

			//Note: after Init or SetSyncDiv you must allow 100 ms for valid new count rate readings
			Sleep(200);

			retcode = PH_GetCountRate(dev[0], 0, &Countrate0);
			if (retcode<0)
			{
				printf("\n ERROR: PH_GetCountRate error %d. Aborted.\n", retcode);
				exit(0);
			}
			else
			{
				// Got TTResult_SyncRate
				_TTResult_SyncRate = Countrate0;
			}

			retcode = PH_GetCountRate(dev[0], 1, &Countrate1);
			if (retcode<0)
			{
				printf("\n ERROR: PH_GetCountRate error %d. Aborted.\n", retcode);
				exit(0);
			}
			else
			{
				// Got _TTResult_InputRate
				_TTResult_InputRate = Countrate1;
			}

			int binsteps = 0;
			retcode = PH_GetBaseResolution(dev[0], &BaseResolution, &binsteps);
			if (retcode < 0)
			{
				printf("\n ERROR: PH_GetBaseResolution error %d. Aborted.\n", retcode);
				exit(0);
			}
			else
			{
				// Got HW_BaseResolution
				_HW_BaseResolution = *((long long*)&BaseResolution);
			}

			printf("\nResolution=%1lf Countrate0=%1d/s Countrate1=%1d/s", Resolution, Countrate0, Countrate1);

			Progress = 0;
			printf("\nProgress:%9d", Progress);

			retcode = PH_StartMeas(dev[0], Tacq);
			if (retcode<0)
			{
				printf("\n ERROR: Error in StartMeas. Aborted.\n");
				exit(0);
			}

			bool isReading = true;

			while (isReading)
			{
				retcode = PH_GetFlags(dev[0], &flags);
				if (retcode<0)
				{
					printf("\nError %1d in GetFlags. Aborted.\n", retcode);
					isReading = false;
				}

				FiFoWasFull = flags&FLAG_FIFOFULL;

				if (FiFoWasFull)
				{
					printf("\nFiFo Overrun!\n");
					PH_StopMeas(dev[0]);
					isReading = false;
				}

				retcode = PH_ReadFiFo(dev[0], buffer, blocksz, &nactual);	//may return less!  
				if (retcode<0)
				{
					printf("\n ERROR: ReadData error %d\n", retcode);
					PH_StopMeas(dev[0]);
					isReading = false;
				}

				if (nactual)
				{
					if (fwrite(buffer, 4, nactual, fp_target_ptu) != (unsigned)nactual)
					{
						printf("\n ERROR: file write error\n");
						PH_StopMeas(dev[0]);
						isReading = false;
					}
					Progress += nactual;
					printf("\b\b\b\b\b\b\b\b\b%9d", Progress);
				}
				else
				{
					retcode = PH_CTCStatus(dev[0], &CTCDone);
					if (retcode<0)
					{
						printf("\n ERROR: Error %1d in StartMeas. Aborted.\n", retcode);
						isReading = false;
					}

					if (CTCDone)
					{
						printf("\nDone\n");
						PH_StopMeas(dev[0]);
						isReading = false;
					}
				}

				//You can query the count rates here, but do it only if you need them
				/*
				retcode = PH_GetCountRate(dev[0],0,&Countrate0);
				if(retcode<0)
				{
				printf("\nPH_GetCountRate error %d. Aborted.\n",retcode);
				}

				retcode = PH_GetCountRate(dev[0],1,&Countrate1);
				if(retcode<0)
				{
				printf("\nPH_GetCountRate error %d. Aborted.\n",retcode);
				}
				*/
			}

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			// Step #4: Update header info
			// Get number of records
			_TTResult_NumberOfRecords = Progress;

			//struct TgHd MeasDesc_Resolution = { "MeasDesc_Resolution", -1, tyFloat8, _MeasDesc_Resolution };
			//struct TgHd HW_BaseResolution = { "HW_BaseResolution", -1, tyFloat8, _HW_BaseResolution };
			//_MeasDesc_GlobalResolution = 1.0 / (_TTResult_SyncRate * 1.0);
			//fseek(fp_target_ptu, HS_MeasDesc_Resolution, SEEK_SET);  write_header(fp_target_ptu, MeasDesc_Resolution, "");
			//fseek(fp_target_ptu, HS_HW_BaseResolution, SEEK_SET); write_header(fp_target_ptu, HW_BaseResolution, "");

			// Update header
			struct TgHd TTResult_SyncRate = { "TTResult_SyncRate", -1, tyInt8, _TTResult_SyncRate };
			struct TgHd TTResult_InputRate = { "TTResult_InputRate", 0, tyInt8, _TTResult_InputRate };
			struct TgHd TTResult_NumberOfRecords = { "TTResult_NumberOfRecords", -1, tyInt8, _TTResult_NumberOfRecords };
			double tempGlobalRes = 1.0 / (_TTResult_SyncRate * 1.0);
			_MeasDesc_GlobalResolution = *((long long*)&tempGlobalRes);
			struct TgHd MeasDesc_GlobalResolution = { "MeasDesc_GlobalResolution", -1, tyFloat8, _MeasDesc_GlobalResolution };
			
			fseek(fp_target_ptu, HS_TTResult_SyncRate, SEEK_SET); write_header(fp_target_ptu, TTResult_SyncRate, "");
			fseek(fp_target_ptu, HS_TTResult_InputRate, SEEK_SET); write_header(fp_target_ptu, TTResult_InputRate, "");
			fseek(fp_target_ptu, HS_MeasDesc_GlobalResolution, SEEK_SET); write_header(fp_target_ptu, MeasDesc_GlobalResolution, "");
			fseek(fp_target_ptu, HS_TTResult_NumberOfRecords, SEEK_SET); write_header(fp_target_ptu, TTResult_NumberOfRecords, "");

			// Close file for each scan
			fclose(fp_target_ptu);
		}
	}
	else {
		printf("\n ERROR: Failed to create folder! Check if program has proper previledges!\n");
		exit(0);
	}
	// Perform multiple continuous scans - END
	/***************************************************************************************************/

	// After all scans are done, move prior stage back to starting point
	// Multiply by -1 to go back to starting point
	totalMovedX = totalMovedX * (-1);
	totalMovedY = totalMovedY * (-1);

	if (prior_xyStage->MoveRelative((double)totalMovedX, (double)totalMovedY) != PRIORLib::Constants::PRIOR_OK) {
		printf("\n ERROR: Failed to move PRIOR stage!\n");
	}

	// Wait until servo stops moving!
	int^ status = 0;
	prior_scan->IsMoving(*status);
	while (*status != 0) {
		Sleep(100);
		prior_scan->IsMoving(*status);
	}

	// Close PicoHarp after all scans are done
	for (int i = 0;i<MAXDEVNUM;i++) //no harm to close all
	{
		PH_CloseDevice(i);
	}

	// Close Prior stage
	if (prior_stage_enabled) {
		prior_scan->DisConnect();
	}

	// Exit program
	exit(0);
	return(0);
}