#pragma once

#define MAX_CONFIG_VARIABLE_LEN 25

// Declare configurable variables for acquistion with default value
int CONF_Acq_Time = 10000;		// 10 Seconds
int CONF_Acq_Mode = 3;			// T3
int CONF_Acq_Binning = 8;		
int CONF_Acq_Offset = 0;
int CONF_Acq_Sync_Divider = 8;
int CONF_Acq_CFD_Zero_Cross_0 = 10;
int CONF_Acq_CFD_Level_0 = 150;
int CONF_Acq_CFD_Zero_Cross_1 = 10;
int CONF_Acq_CFD_Level_1 = 150;
int CONF_Num_Scan_X = 5;
int CONF_Num_Scan_Y = 5;
int CONF_Prior_Enabled = 1;
int CONF_Prior_COMPort_Number = 5;
int CONF_Prior_Start_X = 0;
int CONF_Prior_Start_Y = 0;
int CONF_Prior_Step_X = 50;
int CONF_Prior_Step_Y = 50;

int read_int_from_config_line(char* config_line) {
	char prm_name[MAX_CONFIG_VARIABLE_LEN];
	int val;
	sscanf(config_line, "%s %d\n", prm_name, &val);
	return val;
}