#pragma once
#pragma comment(lib, "rpcrt4.lib")
#pragma warning(disable:4996)
#include  <windows.h>
#include  <dos.h>
#include  <stdio.h>
#include  <conio.h>
#include  <stddef.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <time.h>

extern "C" {
	#include "phdefin.h"
	#include "phlib.h"
	#include "errorcodes.h"
}

unsigned int buffer[TTREADMAX];

// some important Tag Idents (TTagHead.Ident) that we will need to read the most common content of a PTU file
// check the output of this program and consult the tag dictionary if you need more
#define TTTRTagTTTRRecType "TTResultFormat_TTTRRecType"
#define TTTRTagNumRecords  "TTResult_NumberOfRecords"  // Number of TTTR Records in the File;
#define TTTRTagRes         "MeasDesc_Resolution"       // Resolution for the Dtime (T3 Only)
#define TTTRTagGlobRes     "MeasDesc_GlobalResolution" // Global Resolution of TimeTag(T2) /NSync (T3)
#define FileTagEnd         "Header_End"                // Always appended as last tag (BLOCKEND)

// TagTypes  (TTagHead.Typ)
#define tyEmpty8      0xFFFF0008
#define tyBool8       0x00000008
#define tyInt8        0x10000008
#define tyBitSet64    0x11000008
#define tyColor8      0x12000008
#define tyFloat8      0x20000008
#define tyTDateTime   0x21000008
#define tyFloat8Array 0x2001FFFF
#define tyAnsiString  0x4001FFFF
#define tyWideString  0x4002FFFF
#define tyBinaryBlob  0xFFFFFFFF

// RecordTypes
#define rtPicoHarpT3     0x00010303    // (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $03 (T3), HW: $03 (PicoHarp)
#define rtPicoHarpT2     0x00010203    // (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T2), HW: $03 (PicoHarp)
#define rtHydraHarpT3    0x00010304    // (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $03 (T3), HW: $04 (HydraHarp)
#define rtHydraHarpT2    0x00010204    // (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T2), HW: $04 (HydraHarp)
#define rtHydraHarp2T3   0x01010304    // (SubID = $01 ,RecFmt: $01) (V2), T-Mode: $03 (T3), HW: $04 (HydraHarp)
#define rtHydraHarp2T2   0x01010204    // (SubID = $01 ,RecFmt: $01) (V2), T-Mode: $02 (T2), HW: $04 (HydraHarp)
#define rtTimeHarp260NT3 0x00010305    // (SubID = $00 ,RecFmt: $01) (V2), T-Mode: $03 (T3), HW: $05 (TimeHarp260N)
#define rtTimeHarp260NT2 0x00010205    // (SubID = $00 ,RecFmt: $01) (V2), T-Mode: $02 (T2), HW: $05 (TimeHarp260N)
#define rtTimeHarp260PT3 0x00010306    // (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T3), HW: $06 (TimeHarp260P)
#define rtTimeHarp260PT2 0x00010206    // (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T2), HW: $06 (TimeHarp260P)

#define CONFIG_LINE_BUFFER_SIZE 256
#pragma pack(8) //structure alignment to 8 byte boundaries

// A Tag entry
struct TgHd {
	char Ident[32];     // Identifier of the tag
	int Idx;            // Index for multiple tags or -1
	unsigned int Typ;  // Type of tag ty..... see const section
	long long TagValue; // Value of tag.
} TagHead;


// TDateTime (in file) to time_t (standard C) conversion

const int EpochDiff = 25569; // days between 30/12/1899 and 01/01/1970
const int SecsInDay = 86400; // number of seconds in a day

time_t TDateTime_TimeT(double Convertee)
{
	time_t Result((long)(((Convertee)-EpochDiff) * SecsInDay));
	return Result;
}

FILE *fpin, *fpout, *converted_ptu_fpout;
bool IsT2;
long long RecNum;
long long oflcorrection;
long long truensync, truetime;
int m, c;
double GlobRes = 0.0;
double Resolution = 0.0;
unsigned int dlen = 0;
unsigned int cnt_0 = 0, cnt_1 = 0;

// procedures for Photon, overflow, marker

//Got Photon
//  TimeTag: Raw TimeTag from Record * Globalresolution = Real Time arrival of Photon
//  DTime: Arrival time of Photon after last Sync event (T3 only) DTime * Resolution = Real time arrival of Photon after last Sync event
//  Channel: Channel the Photon arrived (0 = Sync channel for T2 measurements)
void GotPhoton(long long TimeTag, int Channel, int DTime)
{
	if (IsT2)
	{
		fprintf(fpout, "%I64u CHN %1x %I64u %8.0lf\n", RecNum, Channel, TimeTag, (TimeTag * GlobRes * 1e12));
	}
	else
	{
		fprintf(fpout, "%I64u CHN %1x %I64u %8.0lf %10u\n", RecNum, Channel, TimeTag, (TimeTag * GlobRes * 1e9), DTime);
	}
}

//Got Marker
//  TimeTag: Raw TimeTag from Record * Globalresolution = Real Time arrival of Photon
//  Markers: Bitfield of arrived Markers, different markers can arrive at same time (same record)
void GotMarker(long long TimeTag, int Markers)
{
	fprintf(fpout, "%I64u MAR %2x %I64u\n", RecNum, Markers, TimeTag);
}

//Got Overflow
//  Count: Some TCSPC provide Overflow compression = if no Photons between overflow you get one record for multiple Overflows
void GotOverflow(int Count)
{
	fprintf(fpout, "%I64u OFL * %2x\n", RecNum, Count);
}

// PicoHarp T3 input
void ProcessPHT3(unsigned int TTTRRecord)
{
	const int T3WRAPAROUND = 65536;
	union
	{
		unsigned int allbits;
		struct
		{
			unsigned numsync : 16;
			unsigned dtime : 12;
			unsigned channel : 4;
		} bits;
		struct
		{
			unsigned numsync : 16;
			unsigned markers : 12;
			unsigned channel : 4;
		} special;
	} Record;

	Record.allbits = TTTRRecord;
	if (Record.bits.channel == 0xF) //this means we have a special record
	{
		if (Record.special.markers == 0) //not a marker means overflow
		{
			GotOverflow(1);
			oflcorrection += T3WRAPAROUND; // unwrap the time tag overflow
		}
		else
		{
			truensync = oflcorrection + Record.bits.numsync;
			m = Record.special.markers;
			GotMarker(truensync, m);
		}
	}
	else
	{
		if (
			(Record.bits.channel == 0) //Should never occur in T3 Mode
			|| (Record.bits.channel>4) //Should not occur with current routers
			)
		{
			printf("\nIllegal Channel: #%1d %1u", dlen, Record.bits.channel);
			fprintf(fpout, "\nillegal channel ");
		}

		truensync = oflcorrection + Record.bits.numsync;
		m = Record.bits.dtime;
		c = Record.bits.channel;
		GotPhoton(truensync, c, m);
		dlen++;
	}
};


void ProcessPHT2(unsigned int TTTRRecord)
{
	const int T2WRAPAROUND = 210698240;
	union
	{
		unsigned int allbits;
		struct
		{
			unsigned time : 28;
			unsigned channel : 4;
		} bits;

	} Record;
	unsigned int markers;
	Record.allbits = TTTRRecord;
	if (Record.bits.channel == 0xF) //this means we have a special record
	{
		//in a special record the lower 4 bits of time are marker bits
		markers = Record.bits.time & 0xF;
		if (markers == 0) //this means we have an overflow record
		{
			oflcorrection += T2WRAPAROUND; // unwrap the time tag overflow
			GotOverflow(1);
		}
		else //a marker
		{
			//Strictly, in case of a marker, the lower 4 bits of time are invalid
			//because they carry the marker bits. So one could zero them out.
			//However, the marker resolution is only a few tens of nanoseconds anyway,
			//so we can just ignore the few picoseconds of error.
			truetime = oflcorrection + Record.bits.time;
			GotMarker(truetime, markers);
		}
	}
	else
	{
		if ((int)Record.bits.channel > 4) //Should not occur
		{
			printf(" Illegal Chan: #%I64u %1u\n", RecNum, Record.bits.channel);
			fprintf(fpout, " illegal chan.\n");
		}
		else
		{
			if (Record.bits.channel == 0) cnt_0++;
			if (Record.bits.channel >= 1) cnt_1++;

			truetime = oflcorrection + Record.bits.time;
			m = Record.bits.time;
			c = Record.bits.channel;
			GotPhoton(truetime, c, m);
		}
	}
}

void ProcessHHT2(unsigned int TTTRRecord, int HHVersion)
{
	const int T2WRAPAROUND_V1 = 33552000;
	const int T2WRAPAROUND_V2 = 33554432;
	union {
		DWORD   allbits;
		struct {
			unsigned timetag : 25;
			unsigned channel : 6;
			unsigned special : 1; // or sync, if channel==0
		} bits;
	} T2Rec;
	T2Rec.allbits = TTTRRecord;

	if (T2Rec.bits.special == 1)
	{
		if (T2Rec.bits.channel == 0x3F) //an overflow record
		{
			if (HHVersion == 1)
			{
				oflcorrection += (unsigned __int64)T2WRAPAROUND_V1;
				GotOverflow(1);
			}
			else
			{
				//number of overflows is stored in timetag
				if (T2Rec.bits.timetag == 0) //if it is zero it is an old style single overflow
				{
					GotOverflow(1);
					oflcorrection += (unsigned __int64)T2WRAPAROUND_V2;  //should never happen with new Firmware!
				}
				else
				{
					oflcorrection += (unsigned __int64)T2WRAPAROUND_V2 * T2Rec.bits.timetag;
					GotOverflow(T2Rec.bits.timetag);
				}
			}
		}

		if ((T2Rec.bits.channel >= 1) && (T2Rec.bits.channel <= 15)) //markers
		{
			truetime = oflcorrection + T2Rec.bits.timetag;
			//Note that actual marker tagging accuracy is only some ns.
			m = T2Rec.bits.channel;
			GotMarker(truetime, m);
		}

		if (T2Rec.bits.channel == 0) //sync
		{
			truetime = oflcorrection + T2Rec.bits.timetag;
			GotPhoton(truetime, 0, 0);
		}
	}
	else //regular input channel
	{
		truetime = oflcorrection + T2Rec.bits.timetag;
		c = T2Rec.bits.channel + 1;
		GotPhoton(truetime, c, 0);
	}

}


void ProcessHHT3(unsigned int TTTRRecord, int HHVersion)
{
	const int T3WRAPAROUND = 1024;
	union {
		DWORD allbits;
		struct {
			unsigned nsync : 10;  // numer of sync period
			unsigned dtime : 15;    // delay from last sync in units of chosen resolution
			unsigned channel : 6;
			unsigned special : 1;
		} bits;
	} T3Rec;
	T3Rec.allbits = TTTRRecord;
	if (T3Rec.bits.special == 1)
	{
		if (T3Rec.bits.channel == 0x3F) //overflow
		{
			//number of overflows is stored in nsync
			if ((T3Rec.bits.nsync == 0) || (HHVersion == 1)) //if it is zero or old version it is an old style single oferflow
			{
				oflcorrection += (unsigned __int64)T3WRAPAROUND;
				GotOverflow(1); //should never happen with new Firmware!
			}
			else
			{
				oflcorrection += (unsigned __int64)T3WRAPAROUND * T3Rec.bits.nsync;
				GotOverflow(T3Rec.bits.nsync);
			}
		}
		if ((T3Rec.bits.channel >= 1) && (T3Rec.bits.channel <= 15)) //markers
		{
			truensync = oflcorrection + T3Rec.bits.nsync;
			//the time unit depends on sync period which can be obtained from the file header
			c = T3Rec.bits.channel;
			GotMarker(truensync, c);
		}
	}
	else //regular input channel
	{
		truensync = oflcorrection + T3Rec.bits.nsync;
		//the nsync time unit depends on sync period which can be obtained from the file header
		//the dtime unit depends on the resolution and can also be obtained from the file header
		c = T3Rec.bits.channel;
		m = T3Rec.bits.dtime;
		GotPhoton(truensync, c, m);
	}
}

// Write header information
unsigned int write_header(FILE* fp, TgHd headerStruct, char headerValue[]) {
	unsigned int written_bytes_len = 0;
	
	// Calculate padding length
	int padding_length = 8 - (strlen(headerValue) % 8);

	// Write header struct & Update written number of bytes (for seeking later)
	fwrite(&headerStruct, sizeof(struct TgHd), 1, fp);
	written_bytes_len = written_bytes_len + sizeof(struct TgHd);
	
	// Write header value
	fwrite(headerValue, 1, strlen(headerValue), fp);
	written_bytes_len = written_bytes_len + strlen(headerValue);
	
	if (strlen(headerValue) > 0) {
		for (int i = 0; i < padding_length; i++) {
			fwrite("\0", 1, 1, fp);
			written_bytes_len++;
		}
	}

	return written_bytes_len;
}