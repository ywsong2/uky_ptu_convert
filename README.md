# Continuous Automatic Multi-scanning System with PicoHarp and Prior Stage

This program uses PicoQuant's PicoHarp 300 with Prior stage to perform continuous scans in a row using an input configuration file.

This program is a research project of Chemistry department at University of Kentucky. Current program supports T3 mode only but it may support T2 mode as well later.

# Input configuration file 

List below shows an input configuration file example. Each line with '#' mark is considered as comment. Each comment explains the name of property, the type of the property, and the unit of the properity.

NUMBER_OF_SCAN_X sets number of continuous scans for the X axis. NUMBER_OF_SCAN_Y sets number of continuous scans for the Y axis. Therefore, multiplication of those two numbers are the total number of scans by the PicoHarp 300. For example, if there are 5 scans for X axis and 5 scans for Y axis, then the total number of scans will be 25 (5 x 5).

Each scan will create PTU file. Each PTU file will have unique sequency number in front of file name. After all scans are done, you can open up all PTU files with SymphoTime software.

You need to find which COM port is used to connect your Prior stage to set PRI_COMPORT_NUBMER. You can find the connected COM port number for the Prior stage from the Device Manager in Windows OS.

```
# Acquisition Time | NUMBER | milliseconds
ACQ_TIME 10000

# Acquisition Mode | NUMBER | 3 = T3 MODE
ACQ_MODE 3

# Acquisition Binning | NUMBER 
ACQ_BINNING 0

# Acquisition Offset | NUMBER
ACQ_OFFSET 0

# Acquisition Sync Divider | NUMBER
ACQ_SYNC_DIVIDER 8

# Acquisition CFD Settings | NUMBERS
ACQ_CFD_ZERO_CROSS_0 10
ACQ_CFD_LEVEL_0 150
ACQ_CFD_ZERO_CROSS_1 10
ACQ_CFD_LEVEL_1 150

# Number of Scans | NUMBERS
NUMBER_OF_SCAN_X 5
NUMBER_OF_SCAN_Y 5

# [PRIOR STAGE SETTTING] Use Prior Stage? (0 means not enabled)
PRI_ENABLED 1

# [PRIOR STAGE SETTTING] COM port number | NUMBERS
PRI_COMPORT_NUBMER 5

# [PRIOR STAGE SETTTING] Starting Location | NUMBERS
PRI_START_X 50011
PRI_START_Y 44613

# [PRIOR STAGE SETTTING] Increasing Steps | NUMBERS
PRI_STEP_X 9020
PRI_STEP_Y 9020
```
# How to run the software

```bash
ph_multiscan.exe config.txt
```
